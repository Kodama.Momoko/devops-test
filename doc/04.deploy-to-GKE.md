# Google Cloud Platform の Kubernetes(GKE) にデプロイ

## Cloud Shell を起動する
  - Cloud Shell は自分のアカウントに紐付いたワークスペースと思えばOK
  - [Cloud Shell をデフォルトの状態にリセットする](https://cloud.google.com/shell/docs/limitations#resetting_cloud_shell_to_default_state) 場合はこのページの手順で実施できる

<img src="image/start-cloud-shell.png" width="70%" />

## Kubernetes クラスタ を作成する

- 起動したCloud Shellでコマンド実行
- リージョンをus-central に設定
  - 現状、たぶん一番安いっぽい

```
$ gcloud config set compute/zone us-central1-f
```

![](image/cloudshell-sample.png)


- GKE のインスタンスを生成
  - 起動できる最小限の構成
    - `g1-small` では Istio や Spinnaker を起動できなかった、、、、
  - 時間単位で課金が起きるもの
    - このハンズオンで、数百円程度はかかる可能性があります
      - 「予算とアラート」を設定しておくと安心
- まず、GUIで、「Kubernetes Engine」のページにアクセスする
  - Kubernetes Engine APIが有効になる（説明にあった、Masterが起動する）
  - GKEではMasterは無料！なので気軽に試せる

- Kubernetes Engineのメニューに遷移
<img src="image/menu-gke.png" width="70%" />

- Kubernetes Engine APIが有効になる
<img src="image/start-k8s.png" width="70%" />


- 以下のコマンドでクラスタを作成

```
$ gcloud container clusters create devops-test \
  --num-nodes 3 \
  --machine-type n1-standard-2
```

- `ERROR: (gcloud.container.clusters.create) ResponseError: code=403, message=Kubernetes Engine API is not enabled for this project. Please ensure it is enabled in Google Cloud Console and try again: visit xxxxx` というエラーで失敗した場合は、Kubernetes Engine APIが有効になっていないので、上記の有効になる手順を実施してから再度コマンドを実行する

- 作成したクラスタの情報を確認（正しく作成できていることを確認）

- Tips: 上記の手順で、 Kubernetesクラスタ を作成した場合は、自動でどのクラスタを使うか／使うための認証情報が Cloud Shell にセットされる
- クラスタを作成したあとに、Cloud Shell を初期化した場合などは、以下の手順で認証情報を取得しなおす

```
$ gcloud container clusters describe devops-test
$ gcloud container clusters get-credentials devops-test
```

- 接続している Kubernetesクラスタ の情報を見る

```
$ kubectl cluster-info
```


## サンプルのコードをデプロイ

- Cloud Shell を開き、先程 Fork したコードをホームディレクトリにクローンする

```
$ cd && \
  git clone https://gitlab.com/{your_name}/devops-test.git
```

- Dockerイメージをビルドして、 Container Registry にプッシュ

```
$ export PROJECT=$(gcloud info --format='value(config.project)') && \
  cd ~/devops-test && \
  docker build -t gcr.io/$PROJECT/devops-test:v0.0.1 web/. && \
  docker push gcr.io/$PROJECT/devops-test:v0.0.1
```

- Container Registry にイメージが登録されていることを確認（GUI）

- Container Registry のメニューに遷移
<img src="image/menu-container-registry.png" width="30%" />

- イメージが登録されている
<img src="image/container-images.png" width="70%" />


- サンプルの設定の「コンテナイメージ名」を修正した設定ファイルを作成する

```
$ export PROJECT=$(gcloud info --format='value(config.project)') && \
  cd ~/devops-test && \
  sed -i s/devops-test/devops-test:v0.0.1/g k8s/deployment* && \
  sed -i s/project_name/$PROJECT/g k8s/deployment* && \
  cat k8s/deployment.yaml
```

- イメージ名が変更になっていることを確認する

```
$ cd ~/devops-test && \
  kubectl apply -f k8s/deployment.yaml && \
  kubectl apply -f k8s/deployment-canary.yaml && \
  kubectl apply -f k8s/service.yaml
```

```
$ kubectl get pod
$ kubectl get service
```

- ブラウザでアクセスできることを確認
  - `kubectl get service` で確認した、EXTERNAL-IP
  - EXTERNAL-IPが発行されるまで少し時間がかかるため、IPが確定するまで何回か `kubectl get service` を実行する
- 何回かアクセスすると、 `production` と `canary` に切り替わる
  - ランダムで切り替わる
  - Istioのセットアップが未だなので、「カナリア環境を使ってみる」といったボタンの機能は動かない


## クリーンアップ

```
$ kubectl delete -f k8s/deployment.yaml && \
  kubectl delete -f k8s/deployment-canary.yaml && \
  kubectl delete -f k8s/service.yaml && \
  kubectl get pod && \
  kubectl get service && \
  git checkout .
```

**おつかれさまでした。次のセクションに進みます。**
